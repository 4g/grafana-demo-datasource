/**
 * 插件接入系统配置页
 * // todo 表单验证
 */
import React, { PureComponent, ChangeEvent } from 'react';
import { DataSourceHttpSettings, Field, Input } from '@grafana/ui';
import { DataSourcePluginOptionsEditorProps, DataSourceSettings } from '@grafana/data';
import { MyDataSourceOptions } from './types';
// import { DataSource } from './DataSource';

interface Props extends DataSourcePluginOptionsEditorProps<MyDataSourceOptions> {}

interface State {}

export class ConfigEditor extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const { options } = this.props;
    return (
      <div>
        <DataSourceHttpSettings
          defaultUrl="http://172.16.8.40:8082"
          dataSourceConfig={options}
          onChange={this.onChange}
          showAccessOptions={true}
        />
        <Field label="Resolution">
          <Input
            name="Resolution"
            value={options.jsonData.resolution || ''}
            onChange={this.onResolutionChange}
            placeholder="Enter a number"
          />
        </Field>
      </div>
    );
  }

  onChange = (config: DataSourceSettings<any, {}>) => {
    this.props.onOptionsChange(config);
    // console.warn('=onChange=',this.props.options)
  };

  onResolutionChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { onOptionsChange, options } = this.props;
    const jsonData = {
      ...options.jsonData,
      resolution: parseFloat(event.target.value),
    };
    onOptionsChange({ ...options, jsonData });
  };
}
