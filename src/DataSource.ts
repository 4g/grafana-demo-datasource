/***
 * 数据帧的统一数据结构
 */
import defaults from 'lodash/defaults';

import {
  DataQueryRequest,
  DataQueryResponse,
  DataSourceApi,
  DataSourceInstanceSettings,
  MutableDataFrame,
  FieldType,
} from '@grafana/data';

import { MyQuery, MyDataSourceOptions, defaultQuery } from './types';

export class DataSource extends DataSourceApi<MyQuery, MyDataSourceOptions> {
  resolution: number;

  constructor(instanceSettings: DataSourceInstanceSettings<MyDataSourceOptions>) {
    super(instanceSettings);
    this.resolution = instanceSettings.jsonData.resolution || 1000.0;
  }
  // 查询数据
  async query(options: DataQueryRequest<MyQuery>): Promise<DataQueryResponse> {
    const { range } = options;
    const from = range!.from.valueOf();
    const to = range!.to.valueOf();

    const data = options.targets.map(target => {
      const query = defaults(target, defaultQuery); // 新入参和默认入参合并
      // 创建数据帧：时间字段、数字字段
      const frame = new MutableDataFrame({
        refId: query.refId, // 告诉 grafana 基于此数据帧进行查询
        fields: [
          { name: 'time', type: FieldType.time },
          { name: 'value', type: FieldType.number },
          // { name: 'time', values: [from, to], type: FieldType.time },
          // { name: 'value', values: [query.constant, query.constant], type: FieldType.number },
        ],
      });
      // 持续时间
      const duration = to - from;
      // 确定各点在时间上的距离
      // const step = duration / 1000;
      const step = duration / this.resolution;
      for (let t = 0; t < duration; t += step) {
        // frame.add({ time: from + t, value: Math.sin((2 * Math.PI * t) / duration) });
        frame.add({ time: from + t, value: Math.sin((2 * Math.PI * query.frequency * t) / duration) });
      }
      return frame;
    });
    return { data };
  }
  // 测试数据源
  async testDatasource() {
    // Implement a health check for your data source.
    return {
      status: 'success',
      message: 'Success',
    };
  }
}
