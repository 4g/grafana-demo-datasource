import { DataSourcePlugin } from '@grafana/data';
import { DataSource } from './DataSource'; // 数据源操作
import { ConfigEditor } from './ConfigEditor'; // 配置数据源
import { QueryEditor } from './QueryEditor'; // 查询模型绑定到表单
import { MyQuery, MyDataSourceOptions } from './types'; // 自定义查询模型

export const plugin = new DataSourcePlugin<DataSource, MyQuery, MyDataSourceOptions>(DataSource)
  .setConfigEditor(ConfigEditor)
  .setQueryEditor(QueryEditor);
