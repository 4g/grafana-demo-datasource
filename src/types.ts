import { DataQuery, DataSourceJsonData } from '@grafana/data';

// 查询模型
export interface MyQuery extends DataQuery {
  queryText?: string;
  constant: number;
  frequency: number; // 频率
}
// 查询条件-默认值
export const defaultQuery: Partial<MyQuery> = {
  constant: 6.5,
  frequency: 1.0,
};

/**
 * 配置数据源-每当值发生变化时，配置编辑器中的表单字段都会调用已注册的侦听器
 * These are options configured for each DataSource instance
 */
export interface MyDataSourceOptions extends DataSourceJsonData {
  path?: string;
  resolution?: number;
}

/**
 * Value that is used in the backend, but never sent over HTTP to the frontend
 */
export interface MySecureJsonData {
  apiKey?: string;
}
